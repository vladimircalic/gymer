import React, { useState, useEffect } from "react";
import Login from "./Login.js";
import Home from "./Home.js";
import history from "./History";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { useCookies } from "react-cookie";
import "./App.scss";

function App() {
  const [loggedIn, setLoggedIn] = useState(false);
  const [loading, setLoading] = useState(true);
  const [cookie, , removeCookie] = useCookies(["name"]);

  useEffect(() => {
    if (cookie.token) {
      setLoading(false);
      setLoggedIn(true);
    } else {
      setLoading(false);
      setLoggedIn(false);
      history.push("/login");
    }
  }, [cookie.token]);

  function handleLogout() {
    removeCookie("token", { path: "/" });
    setLoggedIn(false);
    history.push("/login");
  }

  if (loading) return "Loading...";

  return (
    <div className="App">
      <div className="App-header">
        <h1>Gymer Admin</h1>
      </div>
      <div className="App-body">
        <Router history={history}>
          <Switch>
            <Route exact path="/" render={() => (loggedIn ? <Home handleLogout={handleLogout} /> : <Redirect to="/login" />)} />
            <Route exact path="/login" render={() => (!loggedIn ? <Login setLoggedIn={setLoggedIn} /> : <Redirect to="/" />)} />
            <Route path="*" render={() => <div>Status 404: Not Found</div>} />
          </Switch>
        </Router>
      </div>
    </div>
  );
}

export default App;
