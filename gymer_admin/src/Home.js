import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import Axios from "axios";
import { RiDeleteBin6Line } from "react-icons/ri";
import Templates from "./Templates";

export default function Home({ handleLogout }) {
  const { register, handleSubmit, reset } = useForm();
  const [categories, setCategories] = useState([]);
  const [searchExercise, setSearchExercise] = useState();
  const [exercises, setExercises] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState();
  const [deleteExercise, setDeleteExercise] = useState();
  const [workoutTemplateExercises, setWorkoutTemplateExercises] = useState([]);

  useEffect(() => {
    Axios.get("/api/categories")
      .then((res) => setCategories(res.data))
      .catch((error) => console.log(error));
  }, [deleteExercise]);

  useEffect(() => {
    Axios.get("/api/exercises", {
      params: {
        name: searchExercise,
        category: selectedCategory,
      },
    })
      .then((res) => setExercises(res.data))
      .catch((err) => console.log(err));
  }, [selectedCategory, searchExercise, deleteExercise]);

  function onSubmit(data) {
    Axios.post("/api/exercise", data)
      .then((res) => {
        console.log(res.data);
        reset();
      })
      .catch((error) => {
        console.log(error);
      });
  }

  function handleDeleteExercise(exercise) {
    if (deleteExercise) setDeleteExercise("");
    else setDeleteExercise(exercise);
  }

  function confirmDeleteExercise() {
    Axios.delete(`/api/exercise/${deleteExercise.name}`)
      .then((res) => {
        alert(res.data);
        handleDeleteExercise();
      })
      .catch((error) => console.log(error));
  }

  function addToTemplateExercises(exercise) {
    if (!workoutTemplateExercises.some((we) => we._id === exercise._id))
      setWorkoutTemplateExercises([...workoutTemplateExercises, exercise]);
  }

  function removeTemplateExercise(exercise) {
    setWorkoutTemplateExercises(
      workoutTemplateExercises.filter((templateExercise) => {
        return templateExercise._id !== exercise._id;
      })
    );
  }

  return (
    <div className="home-page">
      <div className="page-header">
        <h1>Home Page</h1>
        <button onClick={handleLogout} className="btn btn-secondary">
          Logout
        </button>
      </div>
      <div className="home-page-body">
        <div className="exercises">
          <form className="add-exercise-form" onSubmit={handleSubmit(onSubmit)}>
            <h3>Add Exercise</h3>
            <div className="form-inputs">
              <input placeholder="Exercise Name" className="form-control" name="name" type="text" ref={register} />
              <input placeholder="Exercise Category" className="form-control" name="category" type="text" ref={register} />
            </div>
            <button className="btn btn-primary">Submit</button>
          </form>
          <div className="show-exercises">
            <h3>Exercises</h3>
            <select
              className="form-control"
              onChange={(e) => {
                setSelectedCategory(e.target.value);
              }}
            >
              <option value="">All</option>
              {categories.map((category) => {
                return <option key={category}>{category}</option>;
              })}
            </select>
            <input
              placeholder="Search Exercises"
              className="form-control"
              onChange={(e) => {
                setSearchExercise(e.target.value);
              }}
            />
            {deleteExercise ? (
              <div className="delete-exercise">
                <h4> Delete {deleteExercise.name} ?</h4>
                <div className="buttons">
                  <button onClick={confirmDeleteExercise} className="btn btn-success">
                    Yes
                  </button>
                  <button onClick={handleDeleteExercise} className="btn btn-danger">
                    No
                  </button>
                </div>
              </div>
            ) : (
              ""
            )}
            <div className="selected-exercises">
              {exercises.map((exercise) => {
                return (
                  <div className="exercise" key={exercise._id}>
                    <p onClick={() => addToTemplateExercises(exercise)}>
                      {exercise.name} <span>/ {exercise.category}</span>
                    </p>
                    <RiDeleteBin6Line size={20} onClick={() => handleDeleteExercise(exercise)} />
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <div className="popular-templates">
          <Templates
            workoutTemplateExercises={workoutTemplateExercises}
            removeTemplateExercise={removeTemplateExercise}
            addToTemplateExercises={addToTemplateExercises}
            setWorkoutTemplateExercises={setWorkoutTemplateExercises}
            categories={categories}
          />
        </div>
      </div>
    </div>
  );
}
