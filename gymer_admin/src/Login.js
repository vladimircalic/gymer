import React from "react";
import { useForm } from "react-hook-form";
import Axios from "axios";
import history from "./History";

export default function Login({ setLoggedIn }) {
  const { register, handleSubmit } = useForm();

  function onSubmit(data) {
    Axios.post("/api/admin-login", data)
      .then((res) => {
        setLoggedIn(true);
        history.push("/");
      })
      .catch((error) => {
        console.log(error);
        alert(error.response.data.error.message);
      });
  }
  return (
    <div className="login-page">
      <div className="page-header">
        <h1>Login Page</h1>
      </div>
      <div className="login-page-body">
        <form onSubmit={handleSubmit(onSubmit)}>
          <h3>Admin Login</h3>
          <div className="form-inputs">
            <input placeholder="E-mail" ref={register} type="email" name="email" className="form-control" />
            <input placeholder="Password" ref={register} type="password" name="password" className="form-control" />
          </div>
          <button className="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  );
}
