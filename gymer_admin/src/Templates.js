import React, { useState, useEffect, useRef } from "react";
import { IoMdClose } from "react-icons/io";
import { useForm } from "react-hook-form";
import Axios from "axios";
import { FaWindowClose } from "react-icons/fa";

export default function AddTemplate({
  workoutTemplateExercises,
  removeTemplateExercise,
  setWorkoutTemplateExercises,
  addToTemplateExercises,
  categories,
}) {
  const [templates, setTemplates] = useState([]);
  const searchTemplateCategory = useRef();
  const { register, handleSubmit, errors, reset } = useForm();
  const [showTemplateInfo, setShowTemplateInfo] = useState(false);
  const [templateInfo, setTemplateInfo] = useState();
  const [templateTags, setTemplateTags] = useState([]);
  const [selectedTag, setSelectedTag] = useState();

  useEffect(() => {
    searchTemplateByCategory();
  }, []);

  function onSubmit(data) {
    const templateWorkout = {
      workoutName: data.templateName,
      workoutTag: selectedTag ? selectedTag : { tagName: data.tagName, tagColor: data.tagColor },
      category: data.category,
      exercises: workoutTemplateExercises,
    };
    Axios.post("/api/popular-workouts", templateWorkout)
      .then((res) => {
        alert(res.data);
        reset();
        setWorkoutTemplateExercises([]);
        setSelectedTag("");
      })
      .catch((error) => console.log(error));
  }

  function searchTemplateByCategory() {
    Axios.get(`/api/popular-workouts/${searchTemplateCategory.current.value}`)
      .then((res) => {
        setTemplates(res.data);
      })
      .catch((error) => console.log(error));
  }

  function handleShowTemplateDetails(template) {
    setShowTemplateInfo(true);
    setTemplateInfo(template);
  }

  function closeTemplateInfo() {
    setShowTemplateInfo(false);
    setTemplateInfo("");
  }

  function deleteTemplate() {
    Axios.delete(`/api/popular-workouts/${templateInfo._id}`)
      .then((res) => {
        alert(res.data);
        closeTemplateInfo();
        searchTemplateByCategory();
      })
      .catch((error) => console.log(error));
  }

  useEffect(() => {
    Axios.get("/api/template-tags")
      .then((res) => setTemplateTags(res.data))
      .catch((error) => console.log(error));
  }, []);

  const handleSelectedTag = (e) => {
    setSelectedTag(
      templateTags.filter((tag) => {
        if (e.target.value === tag.tagName) return tag;
        else return null;
      })[0]
    );
  };

  return (
    <>
      <form className="add-template-form" onSubmit={handleSubmit(onSubmit)}>
        <h3>Add Popular Template</h3>
        <div className="form-inputs">
          <input
            className="form-control"
            placeholder="Template Name"
            type="text"
            name="templateName"
            ref={register({ required: "Required" })}
          />
          {errors.templateName && <span className="error-message">{errors.templateName.message}</span>}
          <div className="workout-tag">
            <select className="form-control" style={{ marginTop: "1rem" }} onChange={(e) => handleSelectedTag(e)}>
              <option>Create New Tag</option>
              {templateTags.map((tag) => {
                return (
                  <option key={tag.tagName} style={{ backgroundColor: tag.tagColor }}>
                    {tag.tagName}
                  </option>
                );
              })}
            </select>
            {!selectedTag && (
              <>
                <input className="form-control" placeholder="Workout Tag" type="text" name="tagName" ref={register} />
                <input
                  className="form-control color-input"
                  type="color"
                  name="tagColor"
                  defaultValue={`#${Math.random().toString(16).substr(-6)}`}
                  ref={register}
                />
              </>
            )}
          </div>
          <input
            className="form-control"
            placeholder="Workout Category"
            type="text"
            name="category"
            ref={register({ required: "Required" })}
          />
          {errors.category && <span className="error-message">{errors.category.message}</span>}
        </div>
        <div className="selected-exercises">
          {workoutTemplateExercises.map((exercise) => {
            return (
              <div className="exercise" key={exercise._id}>
                <p onClick={() => addToTemplateExercises(exercise)}>
                  {exercise.name} <span>/ {exercise.category}</span>
                </p>
                <IoMdClose size={20} onClick={() => removeTemplateExercise(exercise)} />
              </div>
            );
          })}
        </div>
        <button className="btn btn-primary">Submit</button>
      </form>
      <div className="templates-list">
        <div className="templates-list-header">
          <h3>Templates List</h3>
        </div>
        <div className="templates-list-body">
          <div className="templates-search">
            <select className="form-control" ref={searchTemplateCategory}>
              <option value="">All</option>
              {categories.map((category) => {
                return <option key={category}>{category}</option>;
              })}
            </select>
            <button className="btn btn-primary" onClick={searchTemplateByCategory}>
              Search
            </button>
          </div>

          <div className="templates">
            {templates.map((template) => {
              return (
                <div key={template._id} className="template" onClick={() => handleShowTemplateDetails(template)}>
                  <p>
                    {template.workoutName}
                    <span> / {template.category}</span>
                  </p>
                </div>
              );
            })}
          </div>
        </div>
        {showTemplateInfo ? (
          <div className="template-info">
            <div className="template-info-header">
              <h3>{templateInfo.workoutName}</h3>
              <FaWindowClose size={25} onClick={closeTemplateInfo} />
            </div>
            <p style={{ display: "flex", alignItems: "center" }}>
              Tag: {templateInfo.workoutTag.tagName}{" "}
              <input className="form-control color-input" value={templateInfo.workoutTag.tagColor} type="color" readOnly />
            </p>
            <p>Category: {templateInfo.category}</p>
            <p>Exercises:</p>
            <div className="template-exercises">
              {templateInfo.exercises.map((exercise) => {
                return <p key={exercise._id}>{exercise.name}</p>;
              })}
            </div>
            <button className="btn btn-secondary" onClick={deleteTemplate}>
              Delete
            </button>
          </div>
        ) : (
          ""
        )}
      </div>
    </>
  );
}
