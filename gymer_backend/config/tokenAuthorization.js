const jwt = require("jsonwebtoken");
const secret = require("./jwtConfig");
const errors = require("../errors/errors");

const tokenCheck = (req, res, next) => {
  const token = req.cookies.token;
  if (!token) {
    next(errors.NO_TOKEN);
  } else {
    jwt.verify(token, secret, (err, decoded) => {
      if (err) {
        next(errors.INVALID_TOKEN);
      } else {
        req.email = decoded.email;
        next();
      }
    });
  }
};

module.exports = tokenCheck;
