const errors = {
  NOT_FOUND: {
    code: "E40401",
    message: "Resource not found",
    status: 404,
  },

  USER_NOT_FOUND: {
    code: "E40402",
    message: "No user with that email",
    status: 404,
  },
  WORKOUT_NOT_FOUND: {
    code: "E40403",
    message: "Workout doesnt exist",
    status: 404,
  },
  WORKOUT_LOG_NOT_FOUND: {
    code: "E40404",
    message: "Workout log doesnt exist",
    status: 404,
  },
  PR_NOT_FOUND: {
    code: "E40405",
    message: "User Pr doesnt exist",
    status: 404,
  },
  PASSWORD_INCORRECT: {
    code: "E40001",
    message: "Password incorrect",
    status: 400,
  },
  USER_EXISTS: {
    code: "E40002",
    message: "User with that email already exists",
    status: 400,
  },
  NEW_PASSWORD_INCORRECT: {
    code: "E40003",
    message: "Password is same as new Password",
    status: 400,
  },
  NO_TOKEN: {
    code: "E40101",
    message: "Unauthorized: No token provided",
    status: 401,
  },
  INVALID_TOKEN: {
    code: "E40102",
    message: "Unauthorized: Invalid token",
    status: 401,
  },
  USER_ALREADY_LOGGED: {
    code: "E40103",
    message: "Unauthorized: User already logged in",
    status: 401,
  },
  ADMIN_REQUIRED: {
    code: "E40104",
    message: "Unauthorized: Admin required",
    status: 401,
  },
  SERVER_ERROR: {
    code: "E50001",
    message: "Unexpected server error",
    status: 500,
  },
};

module.exports = errors;
