const mongoose = require("mongoose");

const popularWorkoutSchema = new mongoose.Schema({
  workoutName: {
    type: String,
    required: true,
  },
  exercises: {
    type: String,
    type: Array,
    required: true,
  },
  workoutTag: {
    type: Object,
    required: true,
  },
  category: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("PopularWorkout", popularWorkoutSchema);
