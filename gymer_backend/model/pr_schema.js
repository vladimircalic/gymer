const mongoose = require("mongoose");

const prSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  exercise: {
    type: String,
    required: true,
  },
  pr: {
    type: String,
    required: true,
  },
  date: {
    type: String,
    required: true,
  },
  comment: {
    type: String,
    required: false,
  },
});

module.exports = mongoose.model("Pr", prSchema);
