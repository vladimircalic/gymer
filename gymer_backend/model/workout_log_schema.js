const mongoose = require("mongoose");

const workoutLogSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  workoutName: {
    type: String,
    required: true,
  },
  templateId: {
    type: String,
    required: true,
  },
  exercises: {
    type: String,
    type: Array,
    required: true,
  },
  workoutTag: {
    type: Object,
    required: true,
  },
  logDate: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("WorkoutLog", workoutLogSchema);
