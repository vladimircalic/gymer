const mongoose = require("mongoose");

const workoutSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  workoutName: {
    type: String,
    required: true,
  },
  exercises: {
    type: String,
    type: Array,
    required: true,
  },
  workoutTag: {
    type: Object,
    required: true,
  },
  creationDate: {
    type: Date,
    required: true,
    default: Date.now,
  },
});

module.exports = mongoose.model("Workout", workoutSchema);
