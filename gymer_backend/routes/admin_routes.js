const express = require("express");
const router = express.Router();
const User = require("../model/user_schema");
const Exercise = require("../model/exercise_schema");
const PopularWorkout = require("../model/popular_workout_schema");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const secret = require("../config/jwtConfig");
const tokenCheck = require("../config/tokenAuthorization");
const errors = require("../errors/errors");

router.get("/template-tags", tokenCheck, checkAdmin, async (req, res, next) => {
  try {
    const allTags = await PopularWorkout.distinct("workoutTag");
    res.status(200).send(allTags);
  } catch (error) {
    console.log(error);
    next(errors.SERVER_ERROR);
  }
});

router.delete("/popular-workouts/:id", tokenCheck, checkAdmin, async (req, res, next) => {
  try {
    const popularWorkout = await PopularWorkout.findOne({ _id: req.params.id });
    if (!popularWorkout) {
      throw errors.WORKOUT_NOT_FOUND;
    }
    await popularWorkout.remove();
    res.status(200).send("Template deleted");
  } catch (error) {
    next(error ? error : errors.SERVER_ERROR);
    console.log(error);
  }
});

router.get("/popular-workouts", tokenCheck, checkAdmin, async (req, res, next) => {
  try {
    const popularWorkouts = await PopularWorkout.find();
    res.status(200).send(popularWorkouts);
  } catch (error) {
    next(errors.SERVER_ERROR);
    console.log(error);
  }
});

router.post("/popular-workouts", tokenCheck, checkAdmin, async (req, res, next) => {
  const popularWorkout = new PopularWorkout({
    workoutName: req.body.workoutName,
    workoutTag: req.body.workoutTag,
    category: req.body.category,
    exercises: req.body.exercises,
  });
  try {
    await popularWorkout.save();
    res.status(200).send("Workout Template Saved");
    console.log(popularWorkout);
  } catch (error) {
    next(errors.SERVER_ERROR);
    console.log(error);
  }
});

router.delete("/exercise/:name", tokenCheck, checkAdmin, async (req, res, next) => {
  try {
    await Exercise.deleteOne({ name: req.params.name });
    res.status(200).send("Exercise Deleted");
  } catch (error) {
    next(errors.SERVER_ERROR);
    console.log(error);
  }
});

router.post("/exercise", tokenCheck, checkAdmin, async (req, res, next) => {
  const newExercise = new Exercise(req.body);
  try {
    await newExercise.save();
    res.status(200).send("Exercise created");
  } catch (error) {
    next(errors.SERVER_ERROR);
    console.log(error);
  }
});

router.post("/admin-login", async (req, res, next) => {
  let user;
  try {
    //Try to find user in database by email
    user = await User.findOne({ email: req.body.email });
    if (user === null) {
      throw errors.USER_NOT_FOUND;
    }
    if (!user.role) {
      throw errors.ADMIN_REQUIRED;
    }
    //Verify the password of the user
    if (await bcrypt.compare(req.body.password, user.password)) {
      //Asignt token
      const payload = {
        email: user.email,
        userId: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        role: user.role,
      };
      const token = jwt.sign(payload, secret);
      return res.cookie("token", token).status(200).send("Welcome");
    } else throw errors.PASSWORD_INCORRECT;
  } catch (error) {
    return next(error);
  }
});

async function checkAdmin(req, res, next) {
  const token = jwt.verify(req.cookies.token, secret);
  let user;
  try {
    user = await User.findOne({ email: token.email });
    if (user == null) {
      throw errors.USER_NOT_FOUND;
    }
    if (!user.role) {
      throw errors.ADMIN_REQUIRED;
    }
    res.user = user;
    next();
  } catch (error) {
    next(error);
  }
}

module.exports = router;
