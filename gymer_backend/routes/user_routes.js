const express = require("express");
const router = express.Router();
const User = require("../model/user_schema");
const Workout = require("../model/workout_schema");
const WorkoutLog = require("../model/workout_log_schema");
const bcrypt = require("bcrypt");
const { userRegister, changePassword } = require("../validation/user_validation");
const jwt = require("jsonwebtoken");
const secret = require("../config/jwtConfig");
const tokenCheck = require("../config/tokenAuthorization");
const errors = require("../errors/errors");

//Delete user account
router.delete("/user", tokenCheck, findUser, async (req, res, next) => {
  try {
    await Workout.deleteMany({ userId: res.user._id });
    await WorkoutLog.deleteMany({ userId: res.user._id });
    await User.deleteOne({ _id: res.user._id });
    res.status(200).send("User Deleted");
  } catch (error) {
    next(error);
  }
});

//Change user password
router.put("/userpassword", tokenCheck, findUser, passwordValidation, async (req, res, next) => {
  try {
    if (await bcrypt.compare(req.body.password, res.user.password)) {
      if (req.body.password === req.body.newPassword) throw errors.NEW_PASSWORD_INCORRECT;
      res.user.password = await bcrypt.hash(req.body.newPassword, 10);
      await res.user.save();
      res.status(200).send("Password changed successfuly");
    } else throw errors.PASSWORD_INCORRECT;
  } catch (error) {
    next(error);
  }
});

//Update user info
router.put("/user", tokenCheck, findUser, async (req, res, next) => {
  res.user.firstName = req.body.firstName;
  res.user.lastName = req.body.lastName;
  res.user.dateOfBirth = req.body.dateOfBirth;
  res.user.gender = req.body.gender;
  try {
    await res.user.save();
    console.log(res.user);
    res.status(200).send("User info updated");
  } catch (error) {
    next(error);
  }
});

//Get user info
router.get("/user", tokenCheck, findUser, async (req, res, next) => res.status(200).send(res.user));

// Check user token for protected routes
router.get("/checktoken", tokenCheck, (req, res) => res.sendStatus(200));

//Register new user
router.post("/register", checkUserExist, userValidation, async (req, res, next) => {
  try {
    const bcryptedPassword = await bcrypt.hash(req.body.password, 10);
    const user = new User({
      email: req.body.email,

      password: bcryptedPassword,
    });
    await user.save();
    console.log(user);
    res.status(201).send("User created");
  } catch (error) {
    next(errors.SERVER_ERROR);
  }
});

// Login user
router.post("/login", async (req, res, next) => {
  let user;
  try {
    //Try to find user in database by email
    user = await User.findOne({ email: req.body.email });
    if (user == null) {
      throw errors.USER_NOT_FOUND;
    }
    //Verify the password of the user
    if (await bcrypt.compare(req.body.password, user.password)) {
      //Asignt token
      const payload = {
        email: user.email,
        userId: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
      };
      const token = jwt.sign(payload, secret);
      return res.cookie("token", token).status(200).send("Welcome");
    } else throw errors.PASSWORD_INCORRECT;
  } catch (error) {
    return next(error);
  }
});

//Functions

//Check if there is user with requested email in database
async function checkUserExist(req, res, next) {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (user) {
      throw errors.USER_EXISTS;
    }
    next();
  } catch (error) {
    return next(error);
  }
}
//Find user by email from token
async function findUser(req, res, next) {
  const token = jwt.verify(req.cookies.token, secret);
  let user;
  try {
    user = await User.findOne({ email: token.email });
    if (user == null) {
      throw errors.USER_NOT_FOUND;
    }
    res.user = user;
    next();
  } catch (error) {
    next(error);
  }
}
//Validate request
function userValidation(req, res, next) {
  const value = userRegister.validate(req.body);
  if (value.error) {
    const VALIDATION_ERROR = {
      code: 40003,
      status: 400,
      message: value.error.details[0].message,
    };
    next(VALIDATION_ERROR);
  } else {
    next();
  }
}
//Validate change password
function passwordValidation(req, res, next) {
  const value = changePassword.validate(req.body);
  if (value.error) {
    const VALIDATION_ERROR = {
      code: 40003,
      status: 400,
      message: value.error.details[0].message,
    };
    next(VALIDATION_ERROR);
  } else {
    next();
  }
}

module.exports = router;
