const express = require("express");
const router = express.Router();
const Exercise = require("../model/exercise_schema");
const Workout = require("../model/workout_schema");
const PopularWorkout = require("../model/popular_workout_schema");
const WorkoutLog = require("../model/workout_log_schema");
const User = require("../model/user_schema");
const Pr = require("../model/pr_schema");
const { addWorkout, logWorkout } = require("../validation/user_validation");
const tokenCheck = require("../config/tokenAuthorization");
const jwt = require("jsonwebtoken");
const secret = require("../config/jwtConfig");
const errors = require("../errors/errors");

router.get("/popular-workouts/:category", tokenCheck, async (req, res, next) => {
  try {
    const popularWorkouts = await PopularWorkout.find({ category: req.params.category });
    res.status(200).send(popularWorkouts);
  } catch (error) {
    next(errors.SERVER_ERROR);
    console.log(error);
  }
});

//Delete user PR
router.delete("/user-pr/:id", tokenCheck, findUserPr, async (req, res, next) => {
  try {
    await res.userPr.remove();
    res.status(200).send("PR Deleted");
  } catch (error) {
    next(errors.SERVER_ERROR);
    console.log(error);
  }
});

//Edit user PR
router.put("/user-pr/:id", tokenCheck, findUserPr, async (req, res, next) => {
  try {
    res.userPr.exercise = req.body.exercise;
    res.userPr.pr = req.body.pr;
    res.userPr.date = req.body.date;
    res.userPr.comment = req.body.comment;
    await res.userPr.save();
    res.status(200).send("PR Updated");
  } catch (error) {
    next(errors.SERVER_ERROR);
    console.log(error);
  }
});

//Add user PR
router.post("/user-pr", tokenCheck, findUser, async (req, res, next) => {
  const userPr = new Pr({
    userId: res.user._id,
    exercise: req.body.exercise,
    pr: req.body.pr,
    date: req.body.date,
    comment: req.body.comment,
  });
  try {
    await userPr.save();
    res.status(201).send("Pr Created");
  } catch (error) {
    next(errors.SERVER_ERROR);
    console.log(error);
  }
});

//Get user PR's
router.get("/user-prs", tokenCheck, findUser, async (req, res, next) => {
  try {
    userPrs = await Pr.find({ userId: res.user._id });
    res.status(200).send(userPrs);
  } catch (error) {
    next(errors.SERVER_ERROR);
    console.log(error);
  }
});

//User exercises
router.get("/userexercises", tokenCheck, findUser, async (req, res, next) => {
  try {
    const userExercises = await WorkoutLog.find({ userId: res.user.id }).distinct("exercises.name");
    res.status(200).send(userExercises);
  } catch (error) {
    next(errors.SERVER_ERROR);
    console.log(error);
  }
});

//Exercise volume
router.get("/exercisevolume/:name", tokenCheck, findUser, async (req, res, next) => {
  try {
    const workoutsCount = await WorkoutLog.aggregate([
      { $match: { userId: res.user.id, exercises: { $elemMatch: { name: req.params.name } } } },
      {
        $project: {
          logDate: "$logDate",
          exercises: {
            $filter: {
              input: "$exercises",
              as: "exercise",
              cond: { $eq: ["$$exercise.name", req.params.name] },
            },
          },
          _id: 0,
        },
      },
    ]);
    res.status(200).send(workoutsCount);
  } catch (error) {
    next(errors.SERVER_ERROR);
    console.log(error);
  }
});

//Number of each unique workout
router.get("/workoutscount", tokenCheck, findUser, async (req, res, next) => {
  try {
    const workoutsCount = await WorkoutLog.aggregate([
      { $match: { userId: res.user.id } },
      {
        $group: {
          _id: "$templateId",
          count: { $sum: 1 },
          workoutName: { $first: "$workoutName" },
          color: { $first: "$workoutTag.tagColor" },
        },
      },
    ]);
    res.status(200).send(workoutsCount);
  } catch (error) {
    next(errors.SERVER_ERROR);
    console.log(error);
  }
});

//Delete logged workout
router.delete("/workout-log/:id", tokenCheck, findLoggedWorkout, async (req, res, next) => {
  try {
    await res.loggedWorkout.remove();
    res.status(200).send("Workout log deleted");
  } catch (error) {
    next(errors.SERVER_ERROR);
  }
});

//Get logged user workouts
router.get("/workout-logs", tokenCheck, findUser, async (req, res, next) => {
  try {
    const loggedWorkouts = await WorkoutLog.find({ userId: res.user._id });
    res.status(200).send(loggedWorkouts);
  } catch (error) {
    next(errors.SERVER_ERROR);
  }
});

//Update logged workout
router.put("/workout-logs/:id", tokenCheck, findLoggedWorkout, workoutLogValidation, async (req, res, next) => {
  res.loggedWorkout.exercises = req.body.exercises;
  res.loggedWorkout.logDate = req.body.logDate;
  try {
    await res.loggedWorkout.save();
    console.log(res.loggedWorkout);
    res.status(200).send("Workout edited");
  } catch (error) {
    next(errors.SERVER_ERROR);
  }
});

//Log user workout
router.post("/workout-logs", tokenCheck, findUser, workoutLogValidation, async (req, res, next) => {
  const workoutLog = new WorkoutLog({
    userId: res.user._id,
    templateId: req.body.templateId,
    workoutName: req.body.workoutName,
    exercises: req.body.exercises,
    workoutTag: req.body.workoutTag,
    logDate: req.body.logDate,
  });
  try {
    const loggedWorkout = await workoutLog.save();
    console.log(loggedWorkout);
    res.status(201).send("Workout logged");
  } catch (error) {
    next(errors.SERVER_ERROR);
  }
});

//Delete workout
router.delete("/workout/:id", tokenCheck, findWorkout, async (req, res, next) => {
  try {
    await res.workout.remove();
    res.status(200).send("Workout deleted");
  } catch (error) {
    next(errors.SERVER_ERROR);
  }
});

//Update workout
router.put("/workout/:id", tokenCheck, findWorkout, workoutValidation, async (req, res, next) => {
  res.workout.workoutName = req.body.workoutName;
  res.workout.exercises = req.body.exercises;
  res.workout.workoutTag = req.body.workoutTag;
  res.workout.color = req.body.color;
  try {
    await res.workout.save();
    console.log(res.workout);
    res.status(200).send("Workout edited");
  } catch (error) {
    next(errors.SERVER_ERROR);
  }
});

router.get("/tags", tokenCheck, async (req, res, next) => {
  try {
    const allTags = await WorkoutLog.distinct("workoutTag");
    res.status(200).send(allTags);
  } catch (error) {
    console.log(error);
    next(errors.SERVER_ERROR);
  }
});

//Get all workouts by user id
router.get("/workouts", tokenCheck, findUser, async (req, res, next) => {
  var query = {
    userId: res.user._id,
  };
  if (req.query.workoutName) {
    query.workoutName = { $regex: req.query.workoutName, $options: "i" };
  }
  try {
    const UserWorkouts = await Workout.find(query).sort({ creationDate: -1 });
    res.send(UserWorkouts);
  } catch (error) {
    next(errors.SERVER_ERROR);
  }
});

//Add workout
router.post("/workout", tokenCheck, findUser, workoutValidation, async (req, res, next) => {
  const workout = new Workout({
    userId: res.user._id,
    workoutName: req.body.workoutName,
    workoutTag: req.body.workoutTag,
    exercises: req.body.exercises,
  });
  try {
    const newWorkout = await workout.save();
    res.status(201).send("Workout created");
    console.log(newWorkout);
  } catch (error) {
    next(errors.SERVER_ERROR);
  }
});

//Categories
router.get("/categories", tokenCheck, async (req, res, next) => {
  try {
    const allCategories = await Exercise.distinct("category");
    res.send(allCategories);
  } catch (error) {
    next(errors.SERVER_ERROR);
  }
});
//Get exercise by name, by category or get all exercises
router.get("/exercises", async (req, res, next) => {
  var query = {};
  if (req.query.name) {
    query.name = { $regex: req.query.name, $options: "i" };
  }
  if (req.query.category) {
    query.category = req.query.category;
  }
  try {
    const exercises = await Exercise.find(query);
    res.send(exercises);
  } catch (error) {
    next(errors.SERVER_ERROR);
  }
});

//Functions
async function findUser(req, res, next) {
  const token = jwt.verify(req.cookies.token, secret);
  let user;
  try {
    user = await User.findOne({ email: token.email });
    if (user == null) {
      throw errors.USER_NOT_FOUND;
    }
    res.user = user;
    next();
  } catch (error) {
    next(error);
  }
}

function workoutValidation(req, res, next) {
  const value = addWorkout.validate(req.body);
  if (value.error) {
    const VALIDATION_ERROR = {
      code: 40003,
      status: 400,
      message: value.error.details[0].message,
    };
    next(VALIDATION_ERROR);
  } else {
    next();
  }
}
function workoutLogValidation(req, res, next) {
  const value = logWorkout.validate(req.body);
  if (value.error) {
    const VALIDATION_ERROR = {
      code: 40003,
      status: 400,
      message: value.error.details[0].message,
    };
    next(VALIDATION_ERROR);
  } else {
    next();
  }
}

async function findWorkout(req, res, next) {
  let workout;
  try {
    workout = await Workout.findById(req.params.id);
    if (workout == null) {
      throw errors.WORKOUT_NOT_FOUND;
    } else {
      res.workout = workout;
      next();
    }
  } catch (error) {
    next(error);
  }
}
async function findLoggedWorkout(req, res, next) {
  let loggedWorkout;
  try {
    loggedWorkout = await WorkoutLog.findById(req.params.id);
    if (loggedWorkout == null) {
      throw errors.WORKOUT_LOG_NOT_FOUND;
    }
    res.loggedWorkout = loggedWorkout;
    next();
  } catch (error) {
    next(error);
  }
}

async function findUserPr(req, res, next) {
  let userPr;
  try {
    userPr = await Pr.findOne({ _id: req.params.id });
    if (userPr === null) {
      throw errors.PR_NOT_FOUND;
    }
    res.userPr = userPr;
    next();
  } catch (error) {
    next(error);
  }
}

module.exports = router;
