const express = require("express");
const app = express();
const path = require("path");
const port = process.env.PORT || 5000;
const userRouter = require("./routes/user_routes");
const workoutRouter = require("./routes/workout_routes");
const adminRouter = require("./routes/admin_routes");
const mongoose = require("mongoose");
const errors = require("./errors/errors");
const cookieParser = require("cookie-parser");

//Database connection
mongoose.connect("mongodb+srv://noliferop:svesuiste1@gymer-app.zscig.mongodb.net/Gymer", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;
db.on("error", (error) => console.log(error));
db.once("open", () => console.log("Connected to Database Gymer"));

//Application
app.use(express.json());
app.use(express.static(path.join(__dirname, "../gymer_frontend/build")));
app.use(express.static(path.join(__dirname, "../gymer_admin/build")));

app.use(cookieParser());
app.use("/api", userRouter);
app.use("/api", workoutRouter);
app.use("/api", adminRouter);
app.get("/admin", (req, res) => {
  res.sendFile("index.html", { root: path.join(__dirname, "../gymer_admin/build/") });
});
app.get("/**", (req, res) => {
  res.sendFile("index.html", { root: path.join(__dirname, "../gymer_frontend/build/") });
});

//Invalid route handler
app.use((req, res, next) => {
  next(errors.NOT_FOUND);
});
//Error handler
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.send({
    error: {
      code: err.code,
      message: err.message,
    },
  });
});

//App port
app.listen(port, () => console.log(`Gymer Server listening on port ${port}...`));
