const joi = require("@hapi/joi");

const schema = {
  userRegister: joi.object({
    email: joi.string().min(8).max(50).required().email(),
    password: joi.string().min(8).required(),
    confirmPassword: joi.any().valid(joi.ref("password")).required().messages({
      "any.only": "Confirm Password must match Password",
    }),
  }),
  addWorkout: joi.object({
    workoutName: joi.string().required().empty(),
    exercises: joi.array().required().empty(),
    workoutTag: joi.object().required().empty(),
  }),
  logWorkout: joi.object({
    workoutName: joi.string().required().empty(),
    templateId: joi.string().required().empty(),
    workoutTag: joi.object().required().empty(),
    exercises: joi.array().required().empty(),
    logDate: joi.string(),
  }),
  changePassword: joi.object({
    password: joi.string().min(8).required(),
    newPassword: joi.string().min(8).required(),
    confirmNewPassword: joi.any().valid(joi.ref("newPassword")).required().messages({
      "any.only": "Confirm Password must match New Password",
    }),
  }),
};

module.exports = schema;
