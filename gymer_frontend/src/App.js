import React, { useState, useEffect } from "react";
import Header from "./components/common/header/Header";
import Footer from "./components/common/footer/Footer";
import ScrollToTop from "./components/common/ScrollToTop";
import { Container } from "react-bootstrap";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import Login from "./components/unprotected/login/Login";
import Home from "./components/protected/Home";
import Register from "./components/unprotected/register/Register";
import history from "./components/common/History";
import { useCookies } from "react-cookie";
import { Context } from "./components/common/Context";
import ServerResponse from "./components/common/ServerResponse";

function App() {
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [loading, setLoading] = useState(true);
  const [cookie, , removeCookie] = useCookies(["name"]);
  const [loggedIn, setLoggedIn] = useState(false);
  const [serverResponse, setServerResponse] = useState([]);

  useEffect(() => {
    if (cookie.token) {
      setLoading(false);
      setLoggedIn(true);
    } else {
      setLoading(false);
      setLoggedIn(false);
      history.push("/login");
    }
  }, [cookie.token]);

  useEffect(() => {
    window.setTimeout(() => {
      setServerResponse("");
    }, 3000);
  }, [serverResponse]);

  function handleLogout() {
    removeCookie("token", { path: "/" });
    setLoggedIn(false);
    history.push("/login");
  }
  function handleSidebar() {
    setSidebarOpen(!sidebarOpen);
  }
  function handleBackdrop() {
    setSidebarOpen(false);
  }

  if (loading) return "Loading...";

  return (
    <Container fluid>
      <Header handleSidebar={handleSidebar} loggedIn={loggedIn} />
      <Router history={history}>
        <Context.Provider value={{ setLoggedIn, removeCookie, serverResponse, setServerResponse }}>
          <ScrollToTop />
          <ServerResponse serverResponse={serverResponse} setServerResponse={setServerResponse} />
          <Switch>
            <Route path="/login" render={() => (!loggedIn ? <Login setLoggedIn={setLoggedIn} /> : <Redirect to="/" />)} />
            <Route path="/register" render={() => (!loggedIn ? <Register /> : <Redirect to="/" />)} />
            <Route
              path="/"
              render={(props) =>
                loggedIn ? (
                  <Home
                    {...props}
                    handleSidebar={handleSidebar}
                    sidebarOpen={sidebarOpen}
                    handleBackdrop={handleBackdrop}
                    handleLogout={handleLogout}
                  />
                ) : (
                  <Redirect to="/login" />
                )
              }
            />
            <Route path="*" render={() => <div>Status 404: Not Found</div>} />
          </Switch>
        </Context.Provider>
      </Router>
      <Footer />
    </Container>
  );
}

export default App;
