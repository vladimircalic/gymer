import React from "react";
import { MdWarning } from "react-icons/md";
import { FaCheck } from "react-icons/fa";
import { MdClose } from "react-icons/md";

export default function ServerResponse({ serverResponse, setServerResponse }) {
  function clearErrorMessage() {
    setServerResponse("");
  }
  return (
    <div className={`server-message ${serverResponse.error ? "danger" : serverResponse.message ? "success" : "hidden"} `} role="alert">
      <div className="svg">{serverResponse.error ? <MdWarning size={60} /> : <FaCheck size={60} />}</div>
      <div className="server-response">
        <h4>{serverResponse.error ? "Error!" : "Success!"}</h4>
        <span>{serverResponse.error ? serverResponse.error : serverResponse.message}</span>
      </div>
      <MdClose size={20} className="close-error" onClick={clearErrorMessage} />
    </div>
  );
}
