import React from "react";
import { Router, Route } from "react-router-dom";
import "./Header.css";
import { FaDumbbell } from "react-icons/fa";
import { GiHamburgerMenu } from "react-icons/gi";
import history from "../History";

export default function Header({ handleSidebar, loggedIn }) {
  return (
    <Router history={history}>
      <div className="header">
        <Route
          path="/"
          render={() => (
            <div className="header-item">
              <GiHamburgerMenu onClick={handleSidebar} className={`header-hamburger ${!loggedIn ? "hidden" : ""}`} size={50} />
            </div>
          )}
        />
        <div className="header-item">
          <FaDumbbell className="header-app-logo" size={64} />
          <h1 className="header-text">GYMER</h1>
        </div>
      </div>
    </Router>
  );
}
