import React from "react";
import Sidebar from "./sidebar/Sidebar";
import Backdrop from "./backdrop/Backdrop";
import DashboardContainer from "./dashboard/DashboardContainer";
import WorkoutsContainer from "./workouts/WorkoutsContainer";
import WorkoutLogs from "./workoutlogs/WorkoutLogs";
import StartWorkoutContainer from "./workouts/StartWorkoutContainer";
import ReportsContainer from "./reports/ReportsContainer";
import Account from "./account/Account";
import history from "../common/History";
import { Router, Route, Switch, Redirect } from "react-router-dom";

export default function Home({ handleSidebar, sidebarOpen, handleBackdrop, handleLogout }) {
  let backdrop;
  if (sidebarOpen) {
    backdrop = <Backdrop handleBackdrop={handleBackdrop} />;
  }

  return (
    <div className="home-page">
      <Sidebar handleSidebar={handleSidebar} sidebarOpen={sidebarOpen} handleBackdrop={handleBackdrop} />
      {backdrop}
      <div className="main">
        <Router history={history}>
          <Switch>
            <Route exact path="/" render={() => <DashboardContainer />} />
            <Route exact path="/workouts" render={() => <WorkoutsContainer />} />
            <Route
              path="/startworkout"
              render={(props) => (props.location.startWorkoutProps ? <StartWorkoutContainer {...props} /> : <Redirect to="/workouts" />)}
            />
            <Route path="/logs" render={() => <WorkoutLogs />} />
            <Route path="/reports" render={() => <ReportsContainer />} />
            <Route path="/account" render={() => <Account handleLogout={handleLogout} />} />
            <Route path="*" render={() => <div>Status 404: Not Found</div>} />
          </Switch>
        </Router>
      </div>
    </div>
  );
}
