import React, { useState, useEffect, useContext } from "react";
import "./Account.scss";
import { GoKey } from "react-icons/go";
import { AiOutlineUser } from "react-icons/ai";
import { AiFillDelete } from "react-icons/ai";
import { FiLogOut } from "react-icons/fi";
import { Router, Route, Link } from "react-router-dom";
import AccountInfo from "./AccountInfo";
import ChangePassword from "./ChangePassword";
import DeleteAccount from "./DeleteAccount";
import history from "../../common/History";
import Axios from "axios";
import { Context } from "../../common/Context";

export default function Account({ handleLogout }) {
  const [user, setUser] = useState([]);
  const [userWorkouts, setUserWorkouts] = useState([]);
  const { setLoggedIn, removeCookie, setServerResponse } = useContext(Context);

  useEffect(() => {
    Axios.get("/api/user")
      .then((res) => {
        setUser(res.data);
      })
      .catch((error) => console.log(error));
    Axios.get("/api/workout-logs")
      .then((res) => {
        setUserWorkouts(res.data);
      })
      .catch((error) => console.log(error));
  }, []);

  function onSubmit(data, e) {
    if (data.firstName) {
      const updatedUser = {
        firstName: data.firstName,
        lastName: data.lastName,
        dateOfBirth: data.dateOfBirth,
        gender: data.gender,
      };
      Axios.put("/api/user", updatedUser)
        .then((res) => {
          setServerResponse({ message: res.data });
        })
        .catch((error) => {
          setServerResponse({ error: error.response.data.error.message });
        });
    }
    if (data.password) {
      const changePassword = {
        password: data.password,
        newPassword: data.newPassword,
        confirmNewPassword: data.confirmNewPassword,
      };
      Axios.put("/api/userpassword", changePassword)
        .then((res) => {
          setServerResponse({ message: res.data });
        })
        .catch((error) => {
          setServerResponse({ error: error.response.data.error.message });
        });
      e.target.reset();
    }
  }
  function handleDeleteAccount(checked) {
    if (checked) {
      Axios.delete("/api/user")
        .then((res) => {
          setServerResponse({ message: res.data });
          removeCookie("token", { path: "/" });
          setLoggedIn(false);
          history.push("/login");
        })
        .catch((error) => {
          setServerResponse({ error: error.response.data.error.message });
        });
    } else setServerResponse({ error: "Confirm you argee if you want to delete your account" });
  }

  return (
    <Router history={history}>
      <div className="account-page">
        <div className="page-header">
          <h1>My Account</h1>
        </div>
        <div className="account-page-body">
          <div className="side-nav">
            <ul className="side-nav-list">
              <Link to="/account">
                <li className="side-nav-list-item">
                  <AiOutlineUser /> Account Info
                </li>
              </Link>
              <Link to="/account/changepassword">
                <li className="side-nav-list-item">
                  <GoKey />
                  Change Password
                </li>
              </Link>
              <Link to="/account/deleteaccount">
                <li className="side-nav-list-item">
                  <AiFillDelete />
                  Delete Account
                </li>
              </Link>
              <li className="side-nav-list-item" onClick={handleLogout}>
                <FiLogOut /> Logout
              </li>
            </ul>
          </div>
          <div className="account-page-card">
            <Route exact path="/account" render={() => <AccountInfo user={user} userWorkouts={userWorkouts} onSubmit={onSubmit} />} />
            <Route path="/account/changepassword" render={() => <ChangePassword onSubmit={onSubmit} />} />
            <Route path="/account/deleteaccount" render={() => <DeleteAccount handleDeleteAccount={handleDeleteAccount} />} />
          </div>
        </div>
      </div>
    </Router>
  );
}
