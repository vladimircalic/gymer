import React from "react";
import { useForm } from "react-hook-form";
import moment from "moment";

export default function AccountInfo({ user, userWorkouts, onSubmit }) {
  const { register, handleSubmit, errors } = useForm();

  return (
    <>
      <h3>Account Info</h3>
      <div className="personal-info">
        <div className="info-header">
          <h6>Personal Information</h6>
        </div>
        <form className="info-body" onSubmit={handleSubmit(onSubmit)}>
          <p className="info-description">
            Sharing your personal information will help us give you a better and more precice experiance. We will keep your information
            private of course.
          </p>
          <div className="info">
            <div className="info-col">
              <div className="info-input">
                <label>First Name</label>
                <input
                  className="form-control"
                  defaultValue={user ? user.firstName : ""}
                  name="firstName"
                  type="text"
                  ref={register({ required: "Required" })}
                />
                <span className="error-message">{errors.firstName && errors.firstName.message}</span>
              </div>
              <div className="info-input">
                <label>Date of birth</label>
                <input
                  className="form-control"
                  name="dateOfBirth"
                  type="date"
                  ref={register({ required: "Required" })}
                  defaultValue={user ? user.dateOfBirth : ""}
                />
                <span className="error-message">{errors.firstName && errors.firstName.message}</span>
              </div>

              <div className="info-input">
                <label>E-mail</label>
                <p> {user ? user.email : ""} </p>
              </div>
              <div className="info-input">
                <label>User since</label>
                <p> {user ? moment(user.registrationDate).format("MMM Do, YYYY") : ""} </p>
              </div>
            </div>
            <div className="info-col">
              <div className="info-input">
                <label>Last Name</label>
                <input
                  className="form-control"
                  defaultValue={user ? user.lastName : ""}
                  name="lastName"
                  type="text"
                  ref={register({ required: "Required" })}
                />
                <span className="error-message">{errors.firstName && errors.firstName.message}</span>
              </div>
              <div className="info-input">
                <label>Gender</label>
                <input
                  className="form-control"
                  name="gender"
                  type="text"
                  ref={register({ required: "Required" })}
                  defaultValue={user ? user.gender : ""}
                />
                <span className="error-message">{errors.firstName && errors.firstName.message}</span>
              </div>
              <button>Update</button>
            </div>
          </div>
        </form>
      </div>
    </>
  );
}
