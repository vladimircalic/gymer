import React, { useRef } from "react";
import { useForm } from "react-hook-form";
import { GoKey } from "react-icons/go";
import { FaLock } from "react-icons/fa";

export default function ChangePassword({ onSubmit }) {
  const { register, handleSubmit, errors, watch } = useForm();
  const newPassword = useRef({});
  newPassword.current = watch("newPassword", "");
  return (
    <>
      <h3>Change Password</h3>
      <div className="card-data">
        <div className="card-info">
          To change your password please enter your current password and then set a new one. Password must be at least 8 characters long.
        </div>
        <form className="change-password-form" onSubmit={handleSubmit(onSubmit)}>
          <p>Current Password</p>
          <div className="input-group form-group">
            <div className="input-group-prepend">
              <span className="input-group-text">
                <FaLock />
              </span>
            </div>
            <input className="form-control" type="password" name="password" ref={register({ minLength: 8, required: true })} />
          </div>
          {errors.password && errors.password.type === "required" && <span className="error-message">Required</span>}
          {errors.password && errors.password.type === "minLength" && (
            <span className="error-message">Password must have at least 8 characters</span>
          )}
          <p>New Password</p>
          <div className="input-group form-group">
            <div className="input-group-prepend">
              <span className="input-group-text">
                <GoKey />
              </span>
            </div>
            <input className="form-control" type="password" name="newPassword" ref={register({ minLength: 8, required: true })} />
          </div>
          {errors.newPassword && errors.newPassword.type === "required" && <span className="error-message">Required</span>}
          {errors.newPassword && errors.newPassword.type === "minLength" && (
            <span className="error-message">Password must have at least 8 characters</span>
          )}
          <p>Confirm New Password</p>
          <div className="input-group form-group">
            <div className="input-group-prepend">
              <span className="input-group-text">
                <GoKey />
              </span>
            </div>
            <input
              className="form-control"
              type="password"
              name="confirmNewPassword"
              ref={register({
                validate: (value) => value === newPassword.current,
                required: true,
              })}
            />
          </div>
          {errors.confirmNewPassword && errors.confirmNewPassword.type === "required" && <span className="error-message">Required</span>}
          {errors.confirmNewPassword && errors.confirmNewPassword.type === "validate" && (
            <span className="error-message">Passwords do not match</span>
          )}
          <div className="button">
            {" "}
            <button>Change Password</button>
          </div>
        </form>
      </div>
    </>
  );
}
