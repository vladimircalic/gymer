import React, { useState } from "react";

export default function DeleteAccount({ handleDeleteAccount }) {
  const [checked, setChecked] = useState(false);

  function handleInputChange(e) {
    if (e.target.checked) setChecked(true);
    else setChecked(false);
  }
  return (
    <>
      <h3>Delete Account </h3>
      <div className="card-data">
        <div className="card-info">
          By deleting your account you will lose all your saved data such as Workouts and Workout Logs. Once you delete it there is no way
          to retrieve your account.
        </div>
        <div className="confirm-delete-account ">
          <div className="custom-control custom-checkbox">
            <input type="checkbox" className="custom-control-input" id="customCheck" checked={checked} onChange={handleInputChange} />{" "}
            <label className="custom-control-label" htmlFor="customCheck">
              Yes, i agree to delete my account
            </label>
          </div>
          <button onClick={() => handleDeleteAccount(checked)}>Delete</button>
        </div>
      </div>
    </>
  );
}
