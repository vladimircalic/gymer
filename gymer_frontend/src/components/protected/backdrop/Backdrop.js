import React from "react";
import "./Backdrop.css";

export default function Backdrop({ handleBackdrop }) {
  return <div className="backdrop" onClick={handleBackdrop}></div>;
}
