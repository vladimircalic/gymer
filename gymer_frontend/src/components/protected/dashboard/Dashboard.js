import React from "react";
import PrInfo from "./PrInfo";
import { RiFileListLine } from "react-icons/ri";
import { GoCalendar } from "react-icons/go";
import { GiWeightLiftingUp } from "react-icons/gi";
import { FaDumbbell } from "react-icons/fa";
import { FaTrophy } from "react-icons/fa";
import { Link } from "react-router-dom";
import { MdAddCircleOutline } from "react-icons/md";
import { FaPlayCircle } from "react-icons/fa";
import { AiFillFileAdd } from "react-icons/ai";

export default function Dashboard({
  userWorkouts,
  loggedUserWorkouts,
  mostTrainedWorkout,
  latestLoggedWorkout,
  prCollection,
  handleShowPrInfo,
  showPrInfo,
  selectedPr,
  setShowPrInfo,
  handleAddNewPr,
  setSelectedPr,
  categories,
  handleSearchWorkout,
  select,
  popularWorkouts,
  handleAddToTemplates,
}) {
  return (
    <div className="dashboard-page">
      <div className="page-header">
        <h2>Dashboard</h2>
      </div>
      <div className="dashboard-info">
        <Link to="/workouts">
          <div className="info">
            <h2>{userWorkouts.length}</h2>
            <span>Workout Templates</span>
            <RiFileListLine className="info-svg" size={60} />
          </div>
        </Link>
        <Link to="/logs">
          <div className="info">
            <h2>{loggedUserWorkouts.length}</h2>
            <span>Logged Workouts</span>
            <GoCalendar className="info-svg" size={60} />
          </div>
        </Link>
        <div className="info orange">
          <h2>{latestLoggedWorkout ? latestLoggedWorkout.workoutName : "None"}</h2>
          <span>Latest Logged Workout</span>
          <GiWeightLiftingUp className="info-svg" size={60} />
        </div>
        <div className="info orange">
          <h2>{mostTrainedWorkout ? mostTrainedWorkout : "None"}</h2>
          <span>Most trained Workout</span>
          <FaDumbbell className="info-svg" size={60} />
        </div>
      </div>
      <div className="dashboard-body">
        <div className="pr-board">
          <div className="pr-board-header">
            <h3>
              PR BOARD <FaTrophy />
            </h3>
          </div>
          {showPrInfo ? <PrInfo selectedPr={selectedPr} setShowPrInfo={setShowPrInfo} setSelectedPr={setSelectedPr} /> : null}
          <div className="pr-board-body">
            <div className="pr labels">
              <p className="exercise-name">Exercise</p>
              <p className="pr-info">PR</p>
            </div>
            {prCollection.map((pr, index) => {
              return (
                <div className="pr" key={index} onClick={() => handleShowPrInfo(pr)}>
                  <p className="exercise-name">{pr.exercise} </p>
                  <p className="pr-info">{pr.pr}</p>
                </div>
              );
            })}
          </div>
          <div className="add-pr">
            <button onClick={handleAddNewPr}>
              Add new PR <MdAddCircleOutline size={30} />
            </button>
          </div>
        </div>
        <div className="dashboard-news">
          <div className="dashboard-news-header">
            <h3>Popular Workouts Templates</h3>
            <select className="form-control" ref={select} onChange={handleSearchWorkout}>
              {categories.map((category) => {
                return <option key={category}>{category}</option>;
              })}
            </select>
          </div>
          <div className="dashboard-news-body">
            {popularWorkouts.map((workout) => {
              return (
                <div key={workout._id} className="userWorkout">
                  <div className="userWorkout-body">
                    <h3>{workout.workoutName}</h3>
                    <ul>
                      {workout.exercises.map((exercise) => {
                        return (
                          <li key={exercise._id}>
                            <span>{exercise.name}</span>
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                  <div className="userWorkout-buttons">
                    <span onClick={() => handleAddToTemplates(workout)}>
                      <AiFillFileAdd className="button-icon" />
                      Add to Workout Templates
                    </span>
                    <Link to={{ pathname: "/startworkout", startWorkoutProps: { workout: workout } }}>
                      <FaPlayCircle className="button-icon" />
                      Start
                    </Link>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
