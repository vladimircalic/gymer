import React, { useState, useEffect, useRef, useContext } from "react";
import Dashboard from "./Dashboard.js";
import Axios from "axios";
import "./Dashboard.scss";
import history from "../../common/History";
import { Context } from "../../common/Context";

export default function DashboardContainer() {
  const [userWorkouts, setUserWorkouts] = useState([]);
  const [loggedUserWorkouts, setLoggedUserWorkouts] = useState([]);
  const [mostTrainedWorkout, setMostTrainedWorkout] = useState([]);
  const [latestLoggedWorkout, setLatestLoggedWorkout] = useState([]);
  const [prCollection, setPrCollection] = useState([]);
  const [showPrInfo, setShowPrInfo] = useState(false);
  const [selectedPr, setSelectedPr] = useState();
  const [categories, setCategories] = useState([]);
  const select = useRef();
  const [popularWorkouts, setPopularWorkouts] = useState([]);
  const { setServerResponse } = useContext(Context);

  useEffect(() => {
    Axios.get("/api/workouts")
      .then((res) => setUserWorkouts(res.data))
      .catch((error) => setServerResponse({ error: error.response.data.error.message }));
    Axios.get("/api/workout-logs")
      .then((res) => setLoggedUserWorkouts(res.data))
      .catch((error) => setServerResponse({ error: error.response.data.error.message }));
    Axios.get("/api/categories")
      .then((res) => {
        setCategories(res.data);
      })
      .catch((error) => setServerResponse({ error: error.response.data.error.message }));
  }, [setServerResponse]);

  useEffect(() => {
    Axios.get("/api/user-prs")
      .then((res) => setPrCollection(res.data))
      .catch((error) => setServerResponse({ error: error.response.data.error.message }));
  }, [showPrInfo, setServerResponse]);

  useEffect(() => {
    setLatestLoggedWorkout(
      loggedUserWorkouts.sort(function (a, b) {
        return new Date(b.logDate) - new Date(a.logDate);
      })[0]
    );
    const loggedWorkoutsNames = loggedUserWorkouts.map((workout) => {
      return workout.workoutName;
    });
    setMostTrainedWorkout(
      loggedWorkoutsNames
        .sort((a, b) => loggedWorkoutsNames.filter((v) => v === a).length - loggedWorkoutsNames.filter((v) => v === b).length)
        .pop()
    );
  }, [loggedUserWorkouts]);

  function handleShowPrInfo(pr) {
    setShowPrInfo(true);
    setSelectedPr(pr);
  }

  function handleAddNewPr() {
    setShowPrInfo(true);
  }

  useEffect(() => {
    Axios.get(`/api/popular-workouts/${categories[0]}`)
      .then((res) => setPopularWorkouts(res.data))
      .catch((error) => setServerResponse({ error: error.response.data.error.message }));
  }, [categories, setServerResponse]);

  function handleSearchWorkout() {
    Axios.get(`/api/popular-workouts/${select.current.value}`)
      .then((res) => setPopularWorkouts(res.data))
      .catch((error) => setServerResponse({ error: error.response.data.error.message }));
  }

  function handleAddToTemplates(workout) {
    const addWorkout = {
      workoutName: workout.workoutName,
      exercises: workout.exercises,
      color: workout.color,
      workoutTag: workout.workoutTag
    };
    Axios.post("/api/workout", addWorkout)
      .then((res) => {
        history.push("/workouts");
        setServerResponse({ message: res.data });
      })
      .catch((error) => {
        setServerResponse({ error: error.response.data.error.message });
      });
  }

  return (
    <Dashboard
      userWorkouts={userWorkouts}
      loggedUserWorkouts={loggedUserWorkouts}
      mostTrainedWorkout={mostTrainedWorkout}
      latestLoggedWorkout={latestLoggedWorkout}
      prCollection={prCollection}
      showPrInfo={showPrInfo}
      selectedPr={selectedPr}
      handleShowPrInfo={handleShowPrInfo}
      setShowPrInfo={setShowPrInfo}
      handleAddNewPr={handleAddNewPr}
      setSelectedPr={setSelectedPr}
      categories={categories}
      handleSearchWorkout={handleSearchWorkout}
      select={select}
      popularWorkouts={popularWorkouts}
      handleAddToTemplates={handleAddToTemplates}
    />
  );
}
