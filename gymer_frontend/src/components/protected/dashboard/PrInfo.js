import React, { useContext } from "react";
import { useForm } from "react-hook-form";
import { FaWindowClose } from "react-icons/fa";
import Axios from "axios";
import { Context } from "../../common/Context";

export default function PrInfo({ selectedPr, setShowPrInfo, setSelectedPr }) {
  const { register, handleSubmit, errors } = useForm();
  const { setServerResponse } = useContext(Context);

  function handleClosePrInfo() {
    setShowPrInfo(false);
    setSelectedPr("");
  }

  function onSubmit(data) {
    const userPr = {
      exercise: data.exercise,
      pr: data.pr,
      date: data.date,
      comment: data.comment,
    };
    if (selectedPr) {
      Axios.put(`/api/user-pr/${selectedPr._id}`, userPr)
        .then((res) => {
          setServerResponse({ message: res.data });
          handleClosePrInfo();
        })
        .catch((error) => {
          setServerResponse({ error: error.response.data.error.message });
        });
    } else
      Axios.post("/api/user-pr", userPr)
        .then((res) => {
          setServerResponse({ message: res.data });
          handleClosePrInfo();
        })
        .catch((error) => {
          setServerResponse({ error: error.response.data.error.message });
        });
  }

  function handleDeletePr() {
    Axios.delete(`/api/user-pr/${selectedPr._id}`)
      .then((res) => {
        setServerResponse({ message: res.data });
        handleClosePrInfo();
      })
      .catch((error) => {
        setServerResponse({ error: error.response.data.error.message });
      });
  }

  return (
    <div className="selected-pr-info">
      <div className="selected-pr-info-header">
        <h2>PR Info</h2>
        <FaWindowClose size={30} onClick={handleClosePrInfo} className="close-card" />
      </div>
      <form className="selected-pr-info-body" onSubmit={handleSubmit(onSubmit)}>
        <div className="pr-info-field">
          <label>Exercise Name {errors.exercise && <span className="error-message">{errors.exercise.message}</span>}</label>
          <input
            defaultValue={selectedPr ? selectedPr.exercise : ""}
            className="form-control"
            type="text"
            name="exercise"
            ref={register({ required: "Required" })}
          />
        </div>
        <div className="pr-info-field">
          <label>PR descripton {errors.pr && <span className="error-message">{errors.pr.message}</span>}</label>
          <input
            defaultValue={selectedPr ? selectedPr.pr : ""}
            className="form-control"
            type="text"
            name="pr"
            ref={register({ required: "Required" })}
          />
        </div>
        <div className="pr-info-field">
          <label>Date {errors.date && <span className="error-message">{errors.date.message}</span>}</label>
          <input
            defaultValue={selectedPr ? selectedPr.date : ""}
            className="form-control"
            type="date"
            name="date"
            ref={register({ required: "Required" })}
          />
        </div>
        <div className="pr-info-field">
          <label>Comment</label>
          <input defaultValue={selectedPr ? selectedPr.comment : ""} className="form-control" type="text" name="comment" ref={register} />
        </div>
        <button>{selectedPr ? "Edit" : "Submit"}</button>
      </form>
      {selectedPr ? (
        <button onClick={handleDeletePr} className="delete-pr">
          Delete
        </button>
      ) : (
        ""
      )}
    </div>
  );
}
