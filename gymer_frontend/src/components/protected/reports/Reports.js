import React from "react";
import { Bar, Pie, Line } from "react-chartjs-2";

export default function Reports({ data, pieData, handleVolume, exerciseVolume, userExercises, select }) {
  const chartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  };

  return (
    <div className="reports-page">
      <div className="page-header">
        <h1>Reports</h1>
      </div>
      <div className="reports-page-body">
        <div className="chart">
          <div className="chart-bar">
            <p>Workout Volume </p>
            <Bar data={data} options={chartOptions} />
          </div>
          <div className="chart-description">
            <span>
              In weight training, volume is the term used to describe how much work you do, such as total sets and reps you perform with a
              certain weight. In Workout Volume chart you can see total volume of your workouts, in other words the total amount of weight
              lifted per workout.
            </span>
          </div>
        </div>
        <div className="chart">
          <div className="chart-pie">
            <p>Workout Frequency</p>
            <Pie
              data={pieData}
              options={{
                responsive: true,
                maintainAspectRatio: false,
              }}
            ></Pie>
          </div>
          <div className="chart-description">
            <span>
              Workout Frequency chart is showing how much times you did each unique workout in total and also the ratio between them. In the
              future we will expand this feature so you can see weekly and monthly frequency also!{" "}
            </span>
          </div>
        </div>
        <div className="chart">
          <div className="chart-line">
            <p>Exercise Volume </p>
            <select className="form-control" ref={select} onChange={handleVolume}>
              {userExercises.map((exercise) => {
                return <option key={exercise}>{exercise}</option>;
              })}
            </select>
            <Line data={exerciseVolume} options={chartOptions} />
          </div>
          <div className="chart-description">
            <span>
              Exercise Volume is a good way of tracking your progress! It shows total weight lifted on each exercise. Choose an exercise and
              we will show you how it changed with each workout you've done.
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}
