import React, { useState, useEffect, useRef } from "react";
import moment from "moment";
import Reports from "./Reports.js";
import Axios from "axios";
import "./Reports.scss";

export default function ReportsContainer() {
  const [userWorkouts, setUserWorkouts] = useState([]);
  const [workoutVolume, setWorkoutVolume] = useState([]);
  const [pieData, setPieData] = useState({});
  const [exerciseVolume, setExerciseVolume] = useState([]);
  const [userExercises, setUserExercises] = useState([]);
  const select = useRef();

  useEffect(() => {
    Axios.get("/api/workout-logs")
      .then((res) => {
        setUserWorkouts(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    Axios.get("/api/userexercises")
      .then((res) => {
        setUserExercises(res.data);
        select.current.value = res.data[0];
        handleVolume();
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    Axios.get("/api/workoutscount")
      .then((res) => {
        setPieData({
          labels: res.data.map((data) => {
            return data.workoutName;
          }),
          datasets: [
            {
              label: "workout %",
              data: res.data.map((data) => {
                return data.count;
              }),
              backgroundColor: res.data.map((data) => {
                return data.color;
              }),
            },
          ],
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    setWorkoutVolume(
      userWorkouts.map((workout) => {
        workout = workout.exercises
          .map((exercise) => {
            exercise = exercise.sets
              .map((set) => {
                return set.reps * set.kg;
              })
              .reduce((a, b) => a + b, 0);
            return exercise;
          })
          .reduce((a, b) => a + b, 0);
        return workout;
      })
    );
  }, [userWorkouts]);

  const data = {
    labels: userWorkouts.map((workout) => {
      return workout.workoutName;
    }),
    datasets: [
      {
        label: "Volume",
        data: workoutVolume,
        backgroundColor: userWorkouts.map((workout) => {
          return (workout = workout.workoutTag.tagColor);
        }),
      },
    ],
  };

  function handleVolume() {
    Axios.get(`/api/exercisevolume/${select.current.value}`)
      .then((res) => {
        setExerciseVolume({
          labels: res.data.map((data) => {
            return moment(data.logDate).format("MMM Do");
          }),
          datasets: [
            {
              label: `${res.data[0].exercises[0].name} volume per workout`,
              fill: false,
              borderColor: "#368bc7",
              data: res.data.map((data) => {
                return (data = data.exercises
                  .map((exercise) => {
                    return (exercise = exercise.sets
                      .map((set) => {
                        return set.reps * set.kg;
                      })
                      .reduce((a, b) => a + b, 0));
                  })
                  .reduce((a, b) => a + b, 0));
              }),
            },
          ],
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  return (
    <Reports
      data={data}
      pieData={pieData}
      handleVolume={handleVolume}
      exerciseVolume={exerciseVolume}
      userExercises={userExercises}
      select={select}
    />
  );
}
