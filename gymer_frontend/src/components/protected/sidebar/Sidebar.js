import React from "react";
import "./Sidebar.scss";
import { AiOutlineDoubleRight } from "react-icons/ai";
import { AiOutlineDoubleLeft } from "react-icons/ai";
import { MdDashboard } from "react-icons/md";
import { GoCalendar } from "react-icons/go";
import { RiFileListLine } from "react-icons/ri";
import { AiOutlineBarChart } from "react-icons/ai";
import { AiOutlineUser } from "react-icons/ai";
import { Link } from "react-router-dom";

export default function Sidebar({ handleSidebar, sidebarOpen, handleBackdrop }) {
  let arrow;
  if (!sidebarOpen) {
    arrow = <AiOutlineDoubleRight size={40} onClick={handleSidebar} className="logo" />;
  }
  if (sidebarOpen) {
    arrow = <AiOutlineDoubleLeft size={40} onClick={handleSidebar} className="logo" />;
  }
  return (
    <nav className={`sidebar ${!sidebarOpen ? "hidden" : ""}`}>
      <ul className="sidebar-list">
        <li className="sidebar-toggle">{arrow}</li>
        <Link to="/" className="sidebar-link" onClick={handleBackdrop}>
          <li className="sidebar-list-item">
            <span>Dashboard</span>
            <MdDashboard className="logo" size={40} />
          </li>
        </Link>
        <Link to="/workouts" className="sidebar-link" onClick={handleBackdrop}>
          <li className="sidebar-list-item">
            <span>Workouts</span>
            <RiFileListLine className="logo" size={40} />
          </li>
        </Link>
        <Link to="/logs" className="sidebar-link" onClick={handleBackdrop}>
          <li className="sidebar-list-item">
            <span>Workout Logs</span>
            <GoCalendar className="logo" size={40} />
          </li>
        </Link>
        <Link to="/reports" className="sidebar-link" onClick={handleBackdrop}>
          <li className="sidebar-list-item">
            <span>Reports</span>
            <AiOutlineBarChart className="logo" size={40} />
          </li>
        </Link>
        <Link to="/account" className="sidebar-link" onClick={handleBackdrop}>
          <li className="sidebar-list-item">
            <span>Account</span>
            <AiOutlineUser className="logo" size={40} />
          </li>
        </Link>
      </ul>
    </nav>
  );
}
