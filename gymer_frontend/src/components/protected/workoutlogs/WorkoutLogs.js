import React, { useState, useEffect, useContext } from "react";
import "./WorkoutLogs.scss";
import Axios from "axios";
import moment from "moment";
import Calendar from "./WorkoutLogsCalendar";
import { FaShareAlt } from "react-icons/fa";
import { AiFillDelete } from "react-icons/ai";
import { FaCheck } from "react-icons/fa";
import { FaEdit } from "react-icons/fa";
import { MdCancel } from "react-icons/md";
import { Link } from "react-router-dom";
import { Context } from "../../common/Context";

export default function WorkoutLogs() {
  const [loggedWorkouts, setLoggedWorkouts] = useState([]);
  const [displayedWorkouts, setDisplayedWorkouts] = useState([]);
  const [deleteWorkout, setDeleteWorkout] = useState([]);
  const { setServerResponse } = useContext(Context);

  useEffect(() => {
    Axios.get("/api/workout-logs")
      .then((res) => {
        setLoggedWorkouts(
          res.data.sort(function (a, b) {
            return new Date(b.logDate) - new Date(a.logDate);
          })
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  useEffect(() => {
    setDisplayedWorkouts([...loggedWorkouts.slice(0, 5)]);
  }, [loggedWorkouts]);

  function showSelectedWorkout(event) {
    setDisplayedWorkouts(
      loggedWorkouts.filter((loggedWorkout) => {
        if (loggedWorkout._id === event.id) return loggedWorkout;
        else return null;
      })
    );
  }
  function handleDeleteWorkout(loggedWorkout) {
    Axios.delete(`/api/workout-log/${loggedWorkout._id}`)
      .then((res) => {
        setLoggedWorkouts(
          loggedWorkouts.filter((lw) => {
            return lw._id !== loggedWorkout._id;
          })
        );
        setServerResponse({ message: res.data });
      })
      .catch((error) => {
        setServerResponse({ error: error.response.data.error.message });
      });
    setDeleteWorkout("");
  }
  function confirmDelete(loggedWorkout) {
    setDeleteWorkout(loggedWorkout);
  }

  return (
    <div className="workout-logs-page">
      <div className="page-header">
        <h1>Workout Logs</h1>
      </div>
      <div className="workout-logs-body">
        <div className="workouts-grid">
          {displayedWorkouts.map((loggedWorkout) => {
            return (
              <div key={loggedWorkout._id} className="userWorkout">
                <div className={`confirm-delete ${deleteWorkout._id === loggedWorkout._id ? "" : "hidden"}`}>
                  <h5>Are you sure you want to delete this workout log?</h5>
                  <div>
                    <button
                      className="yes-button"
                      onClick={() => {
                        handleDeleteWorkout(loggedWorkout);
                      }}
                    >
                      <FaCheck /> Yes
                    </button>
                    <button className="no-button" onClick={confirmDelete}>
                      <MdCancel /> No
                    </button>
                  </div>
                </div>
                <div className="userWorkout-body">
                  <div className="loggedWorkout-header">
                    <h3>{loggedWorkout.workoutName}</h3>
                    <span className="workout-date">{moment(loggedWorkout.logDate).format("MMM Do, YYYY")}</span>
                  </div>
                  <ul>
                    {loggedWorkout.exercises.map((exercise) => {
                      return (
                        <li key={exercise._id} className="workout-exercise">
                          <div className="exercise-header">{exercise.name}</div>
                          {exercise.sets.map((set, index) => {
                            return (
                              <div key={index} className="set">
                                <span>Set {index + 1}:</span>
                                <span>{set.reps} Reps</span>
                                <span>{set.kg} Kg</span>
                              </div>
                            );
                          })}
                        </li>
                      );
                    })}
                  </ul>
                </div>
                <div className="userWorkout-buttons">
                  <span onClick={() => confirmDelete(loggedWorkout)}>
                    <AiFillDelete className="button-icon" />
                    Delete
                  </span>
                  <Link to={{ pathname: "/startworkout", startWorkoutProps: { loggedWorkout: loggedWorkout } }}>
                    <FaEdit className="button-icon" />
                    Edit
                  </Link>
                  <span>
                    <FaShareAlt className="button-icon" />
                    Share
                  </span>
                </div>
              </div>
            );
          })}
        </div>
        <Calendar loggedWorkouts={loggedWorkouts} showSelectedWorkout={showSelectedWorkout} setDisplayedWorkouts={setDisplayedWorkouts} />
      </div>
    </div>
  );
}
