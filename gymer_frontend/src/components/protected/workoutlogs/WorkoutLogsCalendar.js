import React, { useState, useEffect } from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";

export default function WorkoutLogsCalendar({ loggedWorkouts, showSelectedWorkout, setDisplayedWorkouts }) {
  const localizer = momentLocalizer(moment);
  const [myWorkoutsList, setMyWorkoutsList] = useState([]);
  const [calendarWorkouts, setCalendarWorkouts] = useState([]);
  const [workoutTags, setWorkoutTags] = useState([]);
  useEffect(() => {
    setMyWorkoutsList(
      loggedWorkouts.map((workout) => {
        return (workout = {
          id: workout._id,
          title: workout.workoutName,
          tag: workout.workoutTag.tagName,
          color: workout.workoutTag.tagColor,
          start: moment(workout.logDate).toDate(),
          end: moment(workout.logDate).toDate(),
        });
      })
    );

    let tags = [];
    loggedWorkouts.forEach(function (lw) {
      var i = tags.findIndex((x) => x.name === lw.workoutTag.tagName);
      if (i <= -1) {
        tags.push({ name: lw.workoutTag.tagName, color: lw.workoutTag.tagColor });
      }
    });
    setWorkoutTags(tags);
  }, [loggedWorkouts]);
  useEffect(() => {
    setCalendarWorkouts(myWorkoutsList);
  }, [myWorkoutsList]);

  function eventStyleGetter(event, start, end, isSelected) {
    var style = {
      backgroundColor: event.color,
      borderRadius: "0px",
      opacity: 0.8,
      color: "black",
      border: "0px",
      display: "block",
    };
    return {
      style: style,
    };
  }

  function sortSelectedTag(workoutTag) {
    setCalendarWorkouts(
      myWorkoutsList.filter((workout) => {
        if (workout.tag === workoutTag.name) return workout;
        else return null;
      })
    );
    setDisplayedWorkouts(
      loggedWorkouts
        .filter((workout) => {
          if (workout.workoutTag.tagName === workoutTag.name) return workout;
          else return null;
        })
        .slice(0, 5)
    );
  }
  function showAllWorkouts() {
    setCalendarWorkouts(myWorkoutsList);
    setDisplayedWorkouts([...loggedWorkouts.slice(0, 5)]);
  }

  return (
    <div className="calendar-row">
      <div className="calendar">
        <Calendar
          localizer={localizer}
          events={calendarWorkouts}
          startAccessor="start"
          endAccessor="end"
          style={{ height: 400 }}
          onSelectEvent={(event) => showSelectedWorkout(event)}
          eventPropGetter={eventStyleGetter}
        />
      </div>
      <ul>
        <button onClick={showAllWorkouts}>Show All</button>
        {workoutTags.map((workoutTag, index) => {
          return (
            <button style={{ backgroundColor: workoutTag.color }} key={index} onClick={() => sortSelectedTag(workoutTag)}>
              {workoutTag.name}
            </button>
          );
        })}
      </ul>
    </div>
  );
}
