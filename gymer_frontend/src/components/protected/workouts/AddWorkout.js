import React, { useState, useEffect, useContext } from "react";
import { useForm } from "react-hook-form";
import { FaWindowClose } from "react-icons/fa";
import { FaDumbbell } from "react-icons/fa";
import { FaCheck } from "react-icons/fa";
import { BsChevronDoubleDown } from "react-icons/bs";
import { BsChevronDoubleUp } from "react-icons/bs";
import axios from "axios";
import { Context } from "../../common/Context";

export default function AddWorkout({ handleAddWorkout, editWorkout }) {
  const { register, handleSubmit, errors } = useForm();
  const [exercises, setExercises] = useState([]);
  const [categories, setCategories] = useState([]);
  const [selectedCategories, setSelectedCategories] = useState([]);
  const [workoutExercises, setWorkoutExercises] = useState([]);
  const [search, setSearch] = useState();
  const [showCategories, setShowCategories] = useState(false);
  const { setServerResponse } = useContext(Context);
  const [userTags, setUserTags] = useState([]);
  const [selectedTag, setSelectedTag] = useState();

  useEffect(() => {
    axios
      .get("/api/tags")
      .then((res) => {
        setUserTags(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    axios
      .get("/api/categories")
      .then((res) => {
        setCategories(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  useEffect(() => {
    axios
      .get("/api/exercises", {
        params: {
          name: search,
          category: selectedCategories,
        },
      })
      .then((res) => {
        setExercises(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [selectedCategories, search]);

  useEffect(() => {
    if (editWorkout) {
      setWorkoutExercises(editWorkout.exercises);
    }
  }, [editWorkout]);

  const onSubmit = (data) => {
    const workout = {
      workoutName: editWorkout ? editWorkout.workoutName : data.name,
      workoutTag: selectedTag ? selectedTag : { tagName: data.workoutTag, tagColor: data.tagColor },
      exercises: workoutExercises,
    };
    if (editWorkout) {
      axios
        .put(`/api/workout/${editWorkout._id}`, workout)
        .then((res) => {
          setServerResponse({ message: res.data });
          handleAddWorkout();
        })
        .catch((error) => {
          setServerResponse({ error: error.response.data.error.message });
        });
    } else
      axios
        .post("/api/workout", workout)
        .then((res) => {
          setServerResponse({ message: res.data });
          handleAddWorkout();
        })
        .catch((error) => {
          setServerResponse({ error: error.response.data.error.message });
        });
  };

  const handleSelectedCategory = (category) => {
    if (selectedCategories.includes(category)) {
      setSelectedCategories(
        selectedCategories.filter((selectedCategory) => {
          return selectedCategory !== category;
        })
      );
    } else setSelectedCategories([...selectedCategories, category]);
  };

  const handleWorkoutExercises = (exercise) => {
    if (workoutExercises.some((we) => we._id === exercise._id)) {
      setWorkoutExercises(
        workoutExercises.filter((workoutExercise) => {
          return workoutExercise._id !== exercise._id;
        })
      );
    } else setWorkoutExercises([...workoutExercises, exercise]);
  };

  const handleShowCategories = () => {
    setShowCategories(!showCategories);
  };

  const showWorkoutExercises = () => {
    setExercises(workoutExercises);
  };

  function handleSelectedTag(e) {
    setSelectedTag(
      userTags.filter((tag) => {
        if (e.target.value === tag.tagName) return tag;
        else return null;
      })[0]
    );
  }

  let arrow;
  if (showCategories) arrow = <BsChevronDoubleUp className="arrow-down" onClick={handleShowCategories} size={25} />;
  else arrow = <BsChevronDoubleDown className="arrow-down" onClick={handleShowCategories} size={25} />;

  return (
    <div className="add-workout-card">
      <form onSubmit={handleSubmit(onSubmit)} className="card-form" id="add-workout-form">
        <div className="card-top ">
          {editWorkout ? (
            <div>
              <h3>{editWorkout.workoutName}</h3>
              <h5>Workout Tag</h5>
              <div className="tag-inputs">
                <div className="select-tag">
                  <select
                    onChange={(e) => {
                      handleSelectedTag(e);
                    }}
                    className="form-control"
                  >
                    <option value="">Custom</option>
                    {userTags.map((tag) => {
                      return (
                        <option key={tag.tagName} style={{ backgroundColor: tag.tagColor }}>
                          {tag.tagName}
                        </option>
                      );
                    })}
                  </select>
                </div>
                {!selectedTag ? (
                  <div className="custom-tag">
                    <div>
                      <input
                        className="form-control"
                        type="text"
                        name="workoutTag"
                        defaultValue={editWorkout.workoutTag.tagName}
                        ref={register({ required: "Tag is required" })}
                      />
                      {errors.workoutTag && <span className="error-message">{errors.workoutTag.message}</span>}
                    </div>
                    <input
                      type="color"
                      name="tagColor"
                      defaultValue={editWorkout.workoutTag.tagColor}
                      className="form-control color-input"
                      ref={register}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          ) : (
            <div>
              <h3>Workout name</h3>
              <div className="workout-name">
                <input type="text" name="name" ref={register({ required: "Name is required" })} className="form-control"></input>
                {errors.name && <span className="error-message">{errors.name.message}</span>}
              </div>
              <h5>Workout Tag</h5>
              <div className="tag-inputs">
                <div className="select-tag">
                  <select
                    onChange={(e) => {
                      handleSelectedTag(e);
                    }}
                    name="userTag"
                    className="form-control"
                  >
                    <option value="">Create New Tag</option>
                    {userTags.map((tag) => {
                      return (
                        <option key={tag.tagName} style={{ backgroundColor: tag.tagColor }}>
                          {tag.tagName}
                        </option>
                      );
                    })}
                  </select>
                </div>
                {!selectedTag ? (
                  <div className="custom-tag">
                    <div>
                      <input className="form-control" type="text" name="workoutTag" ref={register({ required: "Tag is required" })} />
                      {errors.workoutTag && <span className="error-message">{errors.workoutTag.message}</span>}
                    </div>
                    <input
                      type="color"
                      name="tagColor"
                      defaultValue={`#${Math.random().toString(16).substr(-6)}`}
                      className="form-control color-input"
                      ref={register}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          )}
          <FaWindowClose size={30} className="close-card" onClick={handleAddWorkout} />
        </div>
        <div className="categories-header">
          <h6>Categories</h6>
          {arrow}
        </div>
        <div className={`card-categories ${showCategories ? "" : "hidden"}`}>
          {categories.map((category, i) => {
            return (
              <button
                type="button"
                key={category}
                value={category}
                className={`category-button category ${selectedCategories.includes(category) ? "active" : ""}`}
                onClick={() => handleSelectedCategory(category)}
              >
                {category}
              </button>
            );
          })}
        </div>
        <h6>Search Exercise</h6>
        <div className="search-input">
          <input
            className="form-control"
            onChange={(e) => {
              setSearch(e.target.value);
            }}
          />
        </div>
        <div className="card-exercises">
          {exercises.map((exercise) => {
            return (
              <li
                value={exercise._id}
                key={exercise._id}
                category={exercise.category}
                className={`exercise ${workoutExercises.some((we) => we._id === exercise._id) ? "active" : ""}`}
                onClick={() => handleWorkoutExercises(exercise)}
              >
                <div>
                  <FaDumbbell />
                  <span>{exercise.name}</span>
                </div>
                <FaCheck size={30} className={`checked ${workoutExercises.some((we) => we._id === exercise._id) ? "" : "hidden"}`} />
              </li>
            );
          })}
        </div>
        <div className="card-form-footer">
          <h6 onClick={showWorkoutExercises} className={`${workoutExercises === exercises ? "active" : ""}`}>
            Selected Exercises:<span>{workoutExercises.length}</span>
          </h6>
          <button className="submit-button">Save Workout</button>
        </div>
      </form>
    </div>
  );
}
