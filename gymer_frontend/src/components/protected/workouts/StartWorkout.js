import React from "react";
import DatePicker from "react-datepicker";
import { useForm } from "react-hook-form";
import { FaRegWindowClose } from "react-icons/fa";
import { MdAddCircleOutline } from "react-icons/md";
import "react-datepicker/dist/react-datepicker.css";
import { IoIosArrowDown } from "react-icons/io";
import { IoIosArrowUp } from "react-icons/io";

export default function StartWorkout({
  userWorkout,
  onSubmit,
  handleAddSet,
  handleRemoveSet,
  handleShowExerciseSets,
  setLogDate,
  workoutExercises,
  showExerciseSets,
  logDate,
}) {
  const { register, handleSubmit, errors } = useForm();
  return (
    <>
      <div className="page-header">
        <h1>{userWorkout.workoutName}</h1>
        <div className="datePicker">
          <DatePicker selected={logDate} onChange={(date) => setLogDate(date)} className="form-control" />
        </div>
      </div>
      <form onSubmit={handleSubmit(onSubmit)}>
        {workoutExercises.map((exercise, eindex) => {
          return (
            <div key={exercise._id} className="userWorkout-exercise">
              <div className="exercise-header" onClick={() => handleShowExerciseSets(eindex)}>
                <h4>{`${exercise.name} ${exercise.sets.length ? "(" + exercise.sets.length + ")" : ""} `} </h4>
                {showExerciseSets[eindex].sets ? <IoIosArrowUp size={30} /> : <IoIosArrowDown size={30} />}
              </div>
              <input hidden={true} name={"exercises[" + eindex + "]._id"} defaultValue={exercise._id} ref={register}></input>
              {exercise.sets
                ? exercise.sets.map((set, index) => {
                    return (
                      <div className={`set ${showExerciseSets[eindex].sets ? "" : "hidden"}`} key={set.id}>
                        <div className="set-number">Set {index + 1}</div>
                        <div className="set-inputs">
                          <div>
                            <input
                              className="form-control"
                              type="text"
                              defaultValue={set.reps ? set.reps : ""}
                              name={`exercises[${eindex}].sets[${index}].reps`}
                              ref={register({ required: "Required" })}
                            />
                            {errors.exercises && errors?.exercises[eindex]?.sets[index]?.reps?.message && (
                              <span className="error-message">{errors.exercises[eindex].sets[index].reps.message}</span>
                            )}
                          </div>
                          <span>Reps</span>
                          <div>
                            <input
                              className="form-control"
                              type="text"
                              defaultValue={set.reps ? set.kg : ""}
                              name={`exercises[${eindex}].sets[${index}].kg`}
                              ref={register({ required: "Required" })}
                            />
                            {errors.exercises && errors?.exercises[eindex]?.sets[index]?.kg?.message && (
                              <span className="error-message">{errors.exercises[eindex].sets[index].kg.message}</span>
                            )}
                          </div>
                          <span>Kg</span>
                        </div>
                        <FaRegWindowClose size={30} onClick={() => handleRemoveSet(exercise, index)} />
                      </div>
                    );
                  })
                : ""}
              <div className="add-set">
                Add Set <MdAddCircleOutline size={30} onClick={() => handleAddSet(exercise, eindex)} />
              </div>
            </div>
          );
        })}
        <button>Save Workout</button>
      </form>
    </>
  );
}
