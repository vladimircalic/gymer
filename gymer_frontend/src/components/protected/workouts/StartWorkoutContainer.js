import React, { useState, useContext } from "react";
import StartWorkout from "./StartWorkout";
import history from "../../common/History";
import { v4 as uuidv4 } from "uuid";
import axios from "axios";
import moment from "moment";
import { Context } from "../../common/Context";

export default function StartWorkoutContainer(props) {
  const { setServerResponse } = useContext(Context);
  const userWorkout = props.location.startWorkoutProps.workout
    ? props.location.startWorkoutProps.workout
    : props.location.startWorkoutProps.loggedWorkout
    ? props.location.startWorkoutProps.loggedWorkout
    : null;
  const [workoutExercises, setWorkoutExercises] = useState(
    userWorkout.exercises.map((exercise) => {
      if (exercise.sets)
        return (exercise = {
          ...exercise,
          sets: exercise.sets.map((set) => {
            return (set = { ...set, id: uuidv4() });
          }),
        });
      else return (exercise = { ...exercise, sets: [] });
    })
  );
  const [logDate, setLogDate] = useState(new Date());
  const [showExerciseSets, setShowExerciseSets] = useState(
    userWorkout.exercises.map((exercise) => {
      return (exercise = { sets: true });
    })
  );

  function handleAddSet(exercise, eindex) {
    setWorkoutExercises(
      workoutExercises.map((we) => {
        if (we._id === exercise._id) we.sets.push({ reps: "", kg: "", id: uuidv4() });
        return we;
      })
    );
    handleShowExerciseSets(eindex, true);
  }

  function handleRemoveSet(exercise, index) {
    setWorkoutExercises(
      workoutExercises.map((we) => {
        if (we._id === exercise._id) we.sets.splice(index, 1);
        return we;
      })
    );
  }

  function handleShowExerciseSets(eindex, opened) {
    setShowExerciseSets(
      showExerciseSets.map((exercise, index) => {
        if (eindex === index && !opened) exercise = { sets: !exercise.sets };
        if (eindex === index && opened) exercise = { sets: true };
        return exercise;
      })
    );
  }

  function onSubmit(data) {
    var nwe = workoutExercises.map((we) => {
      data.exercises.forEach((de) => {
        if (we._id === de._id && de.sets) {
          we.sets = de.sets.filter((set, index) => {
            return index < we.sets.length;
          });
        }
      });
      return we;
    });
    const workoutLog = {
      workoutName: userWorkout.workoutName,
      templateId: userWorkout._id,
      workoutTag: userWorkout.workoutTag,
      exercises: nwe,
      logDate: moment(logDate).format(),
    };
    if (props.location.startWorkoutProps.loggedWorkout) {
      axios
        .put(`/api/workout-logs/${userWorkout._id}`, workoutLog)
        .then((res) => {
          setServerResponse({ message: res.data });
          history.push("/logs");
        })
        .catch((error) => {
          setServerResponse({ error: error.response.data.error.message });
        });
    } else
      axios
        .post("/api/workout-logs", workoutLog)
        .then((res) => {
          setServerResponse({ message: res.data });
          history.push("/logs");
        })
        .catch((error) => {
          setServerResponse({ error: error.response.data.error.message });
        });
    setWorkoutExercises(
      userWorkout.exercises.map((exercise) => {
        const obj = { ...exercise, sets: [] };
        return obj;
      })
    );
  }

  return (
    <div className="startWorkout-page">
      <StartWorkout
        userWorkout={userWorkout}
        onSubmit={onSubmit}
        handleAddSet={handleAddSet}
        handleRemoveSet={handleRemoveSet}
        handleShowExerciseSets={handleShowExerciseSets}
        setLogDate={setLogDate}
        workoutExercises={workoutExercises}
        showExerciseSets={showExerciseSets}
        logDate={logDate}
      />
    </div>
  );
}
