import React from "react";
import { MdAddCircleOutline } from "react-icons/md";
import { FaDumbbell } from "react-icons/fa";
import { AiFillDelete } from "react-icons/ai";
import { FaPlayCircle } from "react-icons/fa";
import { FaEdit } from "react-icons/fa";
import { Link } from "react-router-dom";

export default function Workouts({ userWorkouts, handleEditWorkout, handleDeleteWorkout, handleAddWorkout, setSearchWorkout }) {
  return (
    <div className="workouts-page">
      <div className="page-header">
        <h1>Workout Templates</h1>
        <input
          className="form-control"
          placeholder="Search Workout"
          onChange={(e) => {
            setSearchWorkout(e.target.value);
          }}
        ></input>
      </div>
      <div className="workouts-grid">
        <div className="add-workout">
          <h3>Add workout</h3>
          <MdAddCircleOutline size={120} onClick={() => handleAddWorkout()} className="add-workout-button" />
        </div>
        {userWorkouts.map((userWorkout) => {
          return (
            <div key={userWorkout._id} className="userWorkout">
              <div className="userWorkout-body">
                <h3>
                  <span style={{ backgroundColor: userWorkout.workoutTag.tagColor }}></span> {userWorkout.workoutName}
                </h3>
                <ul>
                  {userWorkout.exercises.map((exercise) => {
                    return (
                      <li key={exercise._id}>
                        <FaDumbbell />
                        <span>{exercise.name}</span>
                      </li>
                    );
                  })}
                </ul>
              </div>
              <div className="userWorkout-buttons">
                <span onClick={() => handleDeleteWorkout(userWorkout)}>
                  <AiFillDelete className="button-icon" />
                  Delete
                </span>
                <span onClick={() => handleEditWorkout(userWorkout)}>
                  <FaEdit className="button-icon" />
                  Edit
                </span>
                <Link to={{ pathname: "/startworkout", startWorkoutProps: { workout: userWorkout } }}>
                  <FaPlayCircle className="button-icon" />
                  Start
                </Link>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
