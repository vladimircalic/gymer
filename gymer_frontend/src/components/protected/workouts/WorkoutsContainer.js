import React, { useState, useEffect, useContext } from "react";
import "./Workouts.scss";
import Workouts from "./Workouts";
import AddWorkout from "./AddWorkout";
import axios from "axios";
import Backdrop from "../backdrop/Backdrop";
import { Context } from "../../common/Context";

export default function WorkoutsContainer() {
  const [addWorkout, setAddWorkout] = useState(false);
  const [userWorkouts, setUserWorkouts] = useState([]);
  const [searchWorkout, setSearchWorkout] = useState();
  const [editWorkout, setEditWorkout] = useState();
  const { setServerResponse } = useContext(Context);

  useEffect(() => {
    axios
      .get("/api/workouts", {
        params: {
          workoutName: searchWorkout,
        },
      })
      .then((res) => {
        setUserWorkouts(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [addWorkout, searchWorkout]);

  function handleAddWorkout() {
    setEditWorkout();
    setAddWorkout(!addWorkout);
  }

  function handleEditWorkout(userWorkout) {
    setEditWorkout(userWorkout);
    setAddWorkout(true);
  }

  function handleDeleteWorkout(userWorkout) {
    axios
      .delete(`/api/workout/${userWorkout._id}`)
      .then((res) => {
        setSearchWorkout("");
        setSearchWorkout();
        setServerResponse({ message: res.data });
      })
      .catch((error) => {
        setServerResponse({ error: error.response.data.error.message });
      });
  }

  let backdrop;
  if (addWorkout) {
    backdrop = <Backdrop />;
  }
  let addOrEditWorkout;
  if (addWorkout) {
    addOrEditWorkout = <AddWorkout handleAddWorkout={handleAddWorkout} editWorkout={editWorkout} />;
  }

  return (
    <div className="workouts-page">
      <Workouts
        userWorkouts={userWorkouts}
        handleEditWorkout={handleEditWorkout}
        handleDeleteWorkout={handleDeleteWorkout}
        handleAddWorkout={handleAddWorkout}
        setSearchWorkout={setSearchWorkout}
      />
      <div className="addWorkout">
        {addOrEditWorkout}
        {backdrop}
      </div>
    </div>
  );
}
