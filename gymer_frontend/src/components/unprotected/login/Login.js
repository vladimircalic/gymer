import React, { useContext } from "react";
import axios from "axios";
import history from "../../common/History";
import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import { FaUserAlt } from "react-icons/fa";
import { GoKey } from "react-icons/go";
import { Button } from "react-bootstrap";
import { Context } from "../../common/Context";

export default function Login({ setLoggedIn }) {
  const { setServerResponse } = useContext(Context);
  const { register, handleSubmit, errors } = useForm();

  function onSubmit(data) {
    const user = {
      email: data.email,
      password: data.password,
    };
    axios
      .post("/api/login", user)
      .then((res) => {
        setServerResponse({ message: res.data });
        setLoggedIn(true);
        history.push("/");
      })
      .catch((error) => {
        if (error) {
          setServerResponse({ error: error.response.data.error.message });
        }
      });
  }

  return (
    <div className="login-page">
      <div className="card">
        <div className="card-header">
          <h3>Login</h3>
        </div>
        <div className="card-body">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="form-group">
              <div className="input-group ">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <FaUserAlt />
                  </span>
                </div>
                <input
                  defaultValue="demoUser@gmail.com"
                  type="text"
                  name="email"
                  className="form-control"
                  placeholder="E-mail"
                  ref={register({
                    required: true,
                    pattern: {
                      value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                      message: "Invalid e-mail address",
                    },
                  })}
                />
              </div>
              {errors.email && errors.email.type === "required" && <span className="error-message">E-mail required</span>}
              {errors.email && <span className="error-message">{errors.email.message}</span>}
            </div>
            <div className="form-group">
              <div className="input-group ">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <GoKey />
                  </span>
                </div>
                <input
                  defaultValue="12345678"
                  type="password"
                  name="password"
                  className="form-control"
                  placeholder="Password"
                  ref={register({ minLength: 8, required: true })}
                />
              </div>
              {errors.password && errors.password.type === "required" && <span className="error-message">Password required</span>}
              {errors.password && errors.password.type === "minLength" && (
                <span className="error-message">Password must have at least 8 characters</span>
              )}
            </div>
            <div className="submit-btn ">
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </div>
          </form>
        </div>
        <div className="card-footer">
          <div className="d-flex justify-content-center links">
            Don't have an account?<Link to="/register">Register here</Link>
          </div>
        </div>
      </div>
    </div>
  );
}
