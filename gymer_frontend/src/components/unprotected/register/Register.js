import React, { useRef, useContext } from "react";
import axios from "axios";
import history from "../../common/History";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import { Context } from "../../common/Context";

export default function Register() {
  const { setServerResponse } = useContext(Context);
  const { register, handleSubmit, errors, watch } = useForm();
  const password = useRef({});
  password.current = watch("password", "");

  function onSubmit(data) {
    const user = {
      email: data.email,
      password: data.password,
      confirmPassword: data.confirmPassword,
    };
    axios
      .post("/api/register", user)
      .then((res) => {
        if (res.status === 201) {
          setServerResponse({ message: res.data });
          history.push("/login");
        }
      })
      .catch((error) => {
        if (error) {
          setServerResponse({ error: error.response.data.error.message });
        }
      });
  }

  return (
    <div className="register-page">
      <div className="card">
        <div className="card-header">
          <h3>Register</h3>
        </div>
        <div className="card-body">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="form-group">
              <div className="input-group ">
                <input
                  type="text"
                  name="email"
                  className="form-control"
                  placeholder="E-mail"
                  ref={register({
                    required: true,
                    pattern: {
                      value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                      message: "Invalid e-mail address",
                    },
                  })}
                />
              </div>
              {errors.email && errors.email.type === "required" && <span className="error-message">E-mail required</span>}
              {errors.email && <span className="error-message">{errors.email.message}</span>}
            </div>
            <div className="form-group">
              <div className="input-group">
                <input
                  type="password"
                  name="password"
                  className="form-control"
                  placeholder="Password"
                  ref={register({ minLength: 8, required: true })}
                />
              </div>
              {errors.password && errors.password.type === "required" && <span className="error-message">Password required</span>}
              {errors.password && errors.password.type === "minLength" && (
                <span className="error-message">Password must have at least 8 characters</span>
              )}
            </div>
            <div className="form-group">
              <div className="input-group ">
                <input
                  type="password"
                  name="confirmPassword"
                  className="form-control"
                  placeholder="Confirm Password"
                  ref={register({
                    validate: (value) => value === password.current,
                    required: true,
                  })}
                />
              </div>
              {errors.confirmPassword && errors.confirmPassword.type === "required" && (
                <span className="error-message">Confirm Password required</span>
              )}
              {errors.confirmPassword && errors.confirmPassword.type === "validate" && (
                <span className="error-message">Passwords do not match</span>
              )}
            </div>
            <div className="submit-btn">
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </div>
          </form>
        </div>
        <div className="card-footer">
          <div className="d-flex justify-content-center links">
            Already have an account?<Link to="/login">Login</Link>
          </div>
        </div>
      </div>
    </div>
  );
}
